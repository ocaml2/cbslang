package main

import (
	"os"
	"fmt"
	"time"
	"context"
	"encoding/binary"
	"strings"
	"strconv"
	"net/http"
	"os/signal"
	"crypto/md5"
	"encoding/json"

	"math/rand"
	"bytes"

	"go.etcd.io/bbolt"
	"github.com/kataras/iris/v12"

	"github.com/lesismal/nbio"
	"github.com/lesismal/nbio/nbhttp"
	"github.com/lesismal/nbio/nbhttp/websocket"
)

var (
	app_db *bbolt.DB
	domain = "http://172.21.32.221:5004"
)

func lease_bucket(bucket string, tx *bbolt.Tx) *bbolt.Bucket {
	location := strings.Split(bucket, "/")

	b := tx.Bucket([]byte(location[0]))

	for i := 1; i < len(location); i++ {
		b = b.Bucket([]byte(location[i]))
	}

	return b
}


func lookup(bucket string, id string) string {
	var res string
	
	app_db.View(func(tx *bbolt.Tx) error {
		b := lease_bucket(bucket, tx)
		res = string(b.Get([]byte(id)))
		return nil		
	})

	return res	
}

type Channel struct {
	Title string `json:"title"`
	Id string `json:"id"`
	Handle string `json:"handle"`
	IsLive bool `json:"islive"`
	Descr string `json:"descr"`
	Hashtags []string `json:"hashtags"`
	Start time.Time `json:"time"`
	Logo string `json:"logo"`
	Feeds []string `json:"feeds"`
}

type Feed struct {
	Name string `json:"name"`
	Main string `json:"key"`
	Seq []string `json:"seq"`
	Source string `json:"source"`
	Down bool `json:"down"`
	IsBroadcast bool `json:"isbroadcast"`
}

type Feed_Sum struct {
	TotalValid int `json:"total"`
	NoName int `json:"unnamed"`
	Duplicate int `json:"duplicate"`
}

func all_channels() []Channel {
	res := []Channel{}
	
	app_db.View(func(tx *bbolt.Tx) error {
		b := lease_bucket("channels", tx)
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var tmp Channel
			json.Unmarshal(v, &tmp)
			res = append(res, tmp)
		}

		return nil
	})

	return res
}

func feed_listing_a(channel Channel) string {
	var res string

	feeds := channel.Feeds

	var feed Feed

	for i, feed_name := range(feeds) {

		_dbres_feed := lookup("feeds", feed_name);
		json.Unmarshal([]byte(_dbres_feed), &feed)

		if i % 2 == 0 {
			res = res + `<div class="columns is-vcentered m-1 is-mobile">`
		}

		var imgsrc string
		var isbroadcast string

		if feed.IsBroadcast {
			imgsrc = domain + "/" + feed.Main
			isbroadcast = "isbroadcast"
		} else {
			imgsrc = feed.Main
		}

		res = res + `<div class="column is-half-mobile card-image" ` + isbroadcast + `>
                               <figure class="image is-4by3" id="` + feed.Name + `" index="0">
                                 <img src="` + imgsrc + `">
                               </figure>
                             </div>`			

		if i % 2 == 1 {
			res = res + `</div>`
		}
		
	}

	return res                     
}

func feed_listing_b(channel Channel) string {
	var res string

	feeds := channel.Feeds

	var feed Feed
	
	for i, feed_name := range(feeds) {
		
		_dbres_feed := lookup("feeds", feed_name);
		json.Unmarshal([]byte(_dbres_feed), &feed)

		if i % 2 == 0 {
			res = res + `<div class="columns is-vcentered m-1 is-mobile">`
		}

		var descr string

		if feed.IsBroadcast {
			descr = ` location="` + channel.Id + "/" + feed.Name + `" isbroadcast `
		} else {
			descr = ` location="` + feed.Source + `" `
		}
	
		res = res + `<div class="column has-text-centered is-half-mobile card-image">
                               <figure class="is-4by3 js-modal-trigger" data-target="video-container">
                                 <video class="feed"` + descr +
				 `id="` + feed.Name + `"></video>
                               </figure>
                               <span class="icon is-large has-text-success sound-toggle">
                                 <i class="fa-solid fa-lg fa-bell-slash"></i>
                               </span>
                              </div>`			

		if i % 2 == 1 {
			res = res + `</div>`
		}
		
	}

	return res                     
}


func channel_listing() string {
	var res string

	channels := all_channels ()

	for i, channel := range(channels) {
		if i % 3 == 0 {
			res = res + "<div class=\"columns is-vcentered m-0 p-0\">"
		}

		feed_listing := feed_listing_a (channel)

		var islive string
		if channel.IsLive {
			islive = `<div class="level-right is-hidden-tablet-only">
                                    <div class="level-item">
                                      <div>
                                        <span class="icon has-text-warning">
                                          <i class="fa-solid fa-tower-broadcast"></i>
                                        </span>
                                        <span>
                                          <small>
                                            <strong class="has-text"> Live Now! </strong>
                                          </small>
                                        </span>
                                      </div>
                                    </div>
                                  </div>`
		}

		var hashtags string
		for _, hashtag := range(channel.Hashtags) {
			hashtags = hashtags + ` <strong class="has-text-primary">` + hashtag + "</strong> "			
		}
		
		res = res + `<div class="column is-one-third">
                               <div class="card m-3" id="` + channel.Id + `">
                                 <a href="/ch/` + channel.Id + `">` +
				   feed_listing + `
                                   <div class="card-content">
                                     <div class="level is-mobile">
                                       <div class="level-left">
                                         <div class="level-item">
                                           <figure class="image is-48x48">
                                             <img src="` + domain + "/" + channel.Logo + `">
                                           </figure>
                                         </div>
                                         <div class="level-item is-vcentered">
                                           <div>
                                             <p class="title is-4">` + channel.Title + `</p>
                                             <p class="heading is-6">` + channel.Handle + `</p>
                                           </div>
                                         </div>
                                       </div>` +
				       islive +
			            `</div>
                                     <div class="content">` +
			               channel.Descr +
			              "<br>" +
			               hashtags +
			              "<br>" +
			              `<time datetime="` + channel.Start.Format(time.DateOnly) +
			                `">` + channel.Start.Format(time.RFC822) + "</time>" +
			            `</div>
                                   </div>
                                 </a>
                               </div>
                             </div>`
		
		if i % 3 == 2 || i +1 == len(channels) {
			res = res + "</div>"
		}
	}
	return res
}

func channel(channel_name string) string {
	var res string

	var channel Channel
	_dbres_chan := lookup("channels", channel_name)
	json.Unmarshal([]byte(_dbres_chan), &channel)

	feed_listing := feed_listing_b(channel)

	res = res + `<div class="level m-4">
                       <div class="level-left">
                         <div class="media-content">
                           <p class="title is-2">` + channel.Title + `</p>
                           <p class="subtitle is-4">` + channel.Handle + `</p>
                         </div>
                       </div>
                     </div>` +
		     feed_listing

	return res
}

func feed(feed_name string) string {
	var feed Feed
	_dbres_feed := lookup("feeds", feed_name);
	json.Unmarshal([]byte(_dbres_feed), &feed)

		
	res := `<div class="columns">
                  <div class="column has-text-centered is-half-mobile card-image">
                    <figure class="is-4by3 js-modal-trigger" data-target="video-container">
                      <video class="feed" location="` + feed.Source +
	                `" id="` + feed.Name + `" controls></video>
                    </figure>
                  </div>
                </div>`

	return res
}

func put(bucket string, key []byte, val []byte) {
	app_db.Update(func(tx *bbolt.Tx) error {
		b := lease_bucket(bucket, tx)
		b.Put(key, val)
		return nil
	})
	return
}

func remove(bucket string, key []byte) {
	app_db.Update(func(tx *bbolt.Tx) error {
		b := lease_bucket(bucket, tx)
		b.Delete(key)
		return nil
	})
	return
}

func put_chat(channel string, message []byte) {

	t := make([]byte, 4)

	app_db.View(func(tx *bbolt.Tx) error {
		b := lease_bucket("chat", tx)
		chatroom := b.Bucket([]byte(channel))

		var v int
		if chatroom == nil {
			v = 0
			
		} else {
			last := chatroom.Get([]byte("t"))
			v = int(binary.BigEndian.Uint32(last))
			v = v +1
		}

		binary.BigEndian.PutUint32(t[0:], uint32(v))
		return nil		
	})

	app_db.Update(func(tx *bbolt.Tx) error {
		b := lease_bucket("chat", tx)
		chatroom, _ := b.CreateBucketIfNotExists([]byte(channel))

		chatroom.Put([]byte("t"), t)

		chatroom.Put(t, message) 

		return nil
	})
	return
}

func get_chat(channel string) []string {
	res := []string{}

	app_db.View(func(tx *bbolt.Tx) error {
		b := lease_bucket("chat", tx)
		chatroom := b.Bucket([]byte(channel))

		if chatroom == nil {
			return nil
		}

		c := chatroom.Cursor()

		i := 0

		for k, v := c.Last(); k != nil; k, v = c.Prev() {
			if i > 10 {
				break
			}
			if bytes.Equal(k, []byte("t")) {
				continue
			}
			i = i +1
			res = append(res, string(v))
		}

		return nil
	})

	return res
}

func put_thumbnail(bucket string, key string, img_data string) {
	var feed Feed
	_dbres_feed := lookup("feeds", key)
	json.Unmarshal([]byte(_dbres_feed), &feed)

	md5 := md5.Sum([]byte(img_data))
	summary := fmt.Sprintf("%x", md5[:])
	img_name := "/images/" + string(summary)

	file, _ := os.Create("resources" + img_name)
	file.Write([]byte(img_data))
	file.Close()

	if strings.Contains(feed.Main, "placeholders") {
		feed.Main = "static" + img_name
	} else if len(feed.Seq) >= 30 {
		prev := feed.Main
		feed.Main = "static" + img_name
		to_del := feed.Seq[0]
		feed.Seq = append(feed.Seq[1:30], prev)
		img_loc := strings.Replace(to_del, "static", "resources", 1)
		os.Remove(img_loc)		
	} else {
		prev := feed.Main
		feed.Main = "static" + img_name
		feed.Seq = append(feed.Seq, prev)
	}

	data, _ := json.Marshal(feed)
	put("feeds", []byte(key), data)
}

func random_feed () []byte {

	var feed_sum *Feed_Sum
	_dbres_feedsum := lookup("feed_sum", "sum")
	json.Unmarshal([]byte(_dbres_feedsum), &feed_sum)

	t := rand.Intn(feed_sum.TotalValid)

	var feed *Feed

	app_db.View(func(tx *bbolt.Tx) error {
		b := lease_bucket("feeds", tx)
		c := b.Cursor()

		i := 0

		for k,v := c.First(); k!= nil; k,v = c.Next() {
			json.Unmarshal(v, &feed)
			
			if i == t {
				break
			} else if feed.Down {
				continue
			}
			i = i+1
		}

		return nil
	})

	var res string;

	origin := strings.Split(feed.Source, "https://")[1]
	domain := strings.Split(origin, "/")[0]
	loc := strings.Split(domain, ".")
	handle := strings.Join(loc[len(loc)-2:len(loc)], ".")

	var feed_down string
	if feed.Down {
		feed_down = "checked"
	} 
	
	res = res +
		`<div class="level is-mobile m-0 p-0">
                   <div class="level-left"></div>
                   <div class="level-right">
                     <div class="level-item">
                       <button class="delete is-large" onclick="feed_deleted();"></button>
                     </div>
                   </div>
                 </div>
                ` +
		`<div class="card m-3" id="` + feed.Name + `">
                   <a href="/f/` + feed.Name + `">
		     <div class="columns">
                       <div class="column is-two-thirds is-offset-2 has-text-centered">
                         <video class="feed" location="` + feed.Source + `" controls></video>
                       </div>
                     </div>
                     <div class="card-content">
                       <div class="level is-mobile">
                         <div class="level-left">
                           <div class="level-item">
                             <figure class="image is-48x48">
                               <img src="` + feed.Main + `">
                             </figure>
                           </div>
                           <div class="level-item is-vcentered">
                             <div>
                               <p class="title is-4">` + feed.Name + `</p>
                               <p class="heading is-6">` + handle + `</p>
                             </div>
                           </div>
                         </div>
                         <div class="level-right">
                           <label class="checkbox">
                             <input type="checkbox" ` + feed_down + ` onchange="feed_down(event)">
                             Stream down?
                           </label>
                         </div>
                       </div>
                       <div class="content">
                         <br>
                         <br>
                         <br>
                       </div>
                     </div>
                   </a>
                 </div>`

	return []byte(res)
}

func ch () {
	var data []byte
	
	/*demo := &Channel {
		Title: "Broadcast Demo",
		Id: "demo",
		Handle: "ccr_media",
		IsLive: true,
		Descr: `Please enjoy your favorite programs courtesy of NBC Charlotte
                        and ABC Los Angeles. Here you can chat with headend engineers
                        and distributors as well as get all of your interesting statistics
                        regarding on-the-minute live feeds.`,
		Hashtags: []string{ "#KABC", "#WCNC" },
		Start: time.Date(2023, time.May, 30, 10, 30, 0, 0, time.UTC),
		Logo: "static/images/ccr.png",
		Feeds: []string{ "abc-la", "nbc-charlotte", "afro", "kalo" },
	}
	data, _ = json.Marshal(demo)
	put("channels", []byte("demo"), data)*/
	remove("channels", []byte("demo"))

	music := &Channel {
		Title: "Musica!",
		Id: "music",
		Handle: "@ceph.tv",
		IsLive: true,
		Descr: `Music from all over the world. Retro classics as well as eclectic
                        and modern pop hits.`,
		Hashtags: []string{ "RetroTV", "CompanyTV" },
		Start: time.Date(2023, time.May, 30, 10, 30, 0, 0, time.UTC),
		Logo: "static/images/music.png",
		Feeds: []string{ "CompanyTV-it", "RetroPlus-cl", "ElSolNetworkTV-ec", "Mas23TV-pa" },
	}
	data, _ = json.Marshal(music)
	put("channels", []byte("music"), data)

	movies := &Channel {
		Title: "Movies!",
		Id: "movies",
		Handle: "@ceph.tv",
		IsLive: true,
		Descr: `American movies in small-format`,
		Hashtags: []string{ "ClassicCinema", "MovieSphere" },
		Start: time.Date(2023, time.May, 30, 10, 30, 0, 0, time.UTC),
		Logo: "static/images/movies.jpg",
		Feeds: []string{ "ClassicCinema-us", "MovieSphere-us" },
	}

	data, _ = json.Marshal(movies)
	put("channels", []byte("movies"), data)

	animals := &Channel {
		Title: "Animals!",
		Id: "animals",
		Handle: "@ceph.tv",
		IsLive: true,
		Descr: `Please enjoy a wide variety of channels actually dedicated to - animals!
                        Cats, dogs, and more! This channel will show you animal-themed content from
                        across the world.`,
		Hashtags: []string{ "PlutoTVAnimals" },
		Start: time.Date(2023, time.May, 30, 10, 30, 0, 0, time.UTC),
		Logo: "static/images/animals.jpg",
		Feeds: []string{ "Cats247-us", "Dogs247-us", "PlutoTVAnimals-us", "PlutoTVAnimalsUK-uk" },
	}
	
	data, _ = json.Marshal(animals)
	put("channels", []byte("animals"), data)

}


/*

 █████ ██████   █████ █████ ███████████
░░███ ░░██████ ░░███ ░░███ ░█░░░███░░░█
 ░███  ░███░███ ░███  ░███ ░   ░███  ░ 
 ░███  ░███░░███░███  ░███     ░███    
 ░███  ░███ ░░██████  ░███     ░███    
 ░███  ░███  ░░█████  ░███     ░███    
 █████ █████  ░░█████ █████    █████   
░░░░░ ░░░░░    ░░░░░ ░░░░░    ░░░░░    
                                       
*/

func init_db () *bbolt.DB {
	db, _ := bbolt.Open("ciskey.db", 0640, nil)

	buckets := []string{
		"channels",
		"feeds",
		"feed_sum",
		"chat",
	}
	
	db.Update(func(tx *bbolt.Tx) error {
		for _, bucket := range buckets {
			location := strings.Split(bucket, "/")

			db_loc, _ := tx.CreateBucketIfNotExists([]byte(location[0]))
			for i := 1; i < len(location); i++ {
				db_loc, _ = db_loc.CreateBucketIfNotExists([]byte(location[i]))
			}			
		}
		return nil
	})

	return db
}

func init_app () *iris.Application {

	app := iris.Default()

	templates := iris.HTML("./templates", ".html").Reload(true)

	app.RegisterView(templates)

	app.HandleDir("/static", iris.Dir("./resources"))

	app.Get("/", func(ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)

		ctx.ViewData("CHANNELS", channel_listing())
		ctx.View("home.html")
		return
	})

	app.Get("/admin", func(ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)
		ctx.View("admin.html")
		return
	})

	app.Get("/about", func(ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)
		ctx.View("about.html")
		return
	})

	app.Get("/signup", func(ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)
		ctx.View("signup.html")
		return
	})


	app.Get("/ch/{channel}", func(ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")

		channel_name := ctx.Params().Get("channel")

		ctx.ViewData("CHANNEL", channel(channel_name))
		ctx.ViewData("CHANNEL_NAME", channel_name);
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)
		ctx.View("channel.html")
		return
	})

	app.Get("/f/{feed}", func (ctx iris.Context) {
		user := ctx.GetCookie("user")
		vmode := ctx.GetCookie("vmode")

		feed_name := ctx.Params().Get("feed")

		ctx.ViewData("FEED", feed(feed_name))
		ctx.ViewData("FEED_NAME", feed_name)
		ctx.ViewData("USER", user)
		ctx.ViewData("VMODE", vmode)
		ctx.View("feed.html")
		return
	})

	return app
}

var active_overlay_subscriptions = map[string]*websocket.Conn {}
var active_chat_subscriptions = map[string]map[string]*websocket.Conn {}

func overlay_subscription(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.NewUpgrader()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	upgrader.OnClose(func(c *websocket.Conn, err error) {
		delete(active_overlay_subscriptions, c.RemoteAddr().String())
	})

	underlying_connection, _ := upgrader.Upgrade(w, r, nil)
	subscription_client := underlying_connection.(*websocket.Conn)

	active_overlay_subscriptions[subscription_client.RemoteAddr().String()] = subscription_client
}

func thumbnail_subscription(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.NewUpgrader()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	
	upgrader.OnMessage(func(c *websocket.Conn, messagetype websocket.MessageType, data []byte) {

		datums := strings.Split(string(data), ",")
		channel_id := strings.Split(datums[0], "=")[1]
		feed_name := strings.Split(datums[1], "=")[1]		
		index, _ := strconv.Atoi(strings.Split(datums[2], "=")[1])

		var feed Feed
		_dbres_feed := lookup("feeds", feed_name)
		json.Unmarshal([]byte(_dbres_feed), &feed)

		if index >= len(feed.Seq) -1 {
			index = 0
		} else {
			index = index +1
		}

		if len(feed.Seq) > 0 {
			var res string
			res = res + "channel=" + channel_id + ","
			res = res + "feed="    + feed_name + ","
			res = res + "index="   + strconv.Itoa(index) + "," 
			res = res + domain + "/" + feed.Seq[index]
			c.WriteMessage(messagetype, []byte(res))
		}
	})

	conn, _ := upgrader.Upgrade(w, r, nil)
	wsConn := conn.(*websocket.Conn)
	wsConn.SetReadDeadline(time.Time{})	
}

func engage(data []byte, channel string, origin string){
	for _, subscription_client := range active_chat_subscriptions[channel] {
		subscription_client.WriteMessage(websocket.TextMessage, data)
	}	
}

func chat_subscription(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.NewUpgrader()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	channel := strings.Join(strings.Split(r.URL.Path, "/")[2:], "/")

	upgrader.OnMessage(func(c *websocket.Conn, messagetype websocket.MessageType, data []byte) {
		put_chat(channel, data)
		engage(data, channel, c.RemoteAddr().String())
	})
	
	upgrader.OnClose(func(c *websocket.Conn, err error) {
		delete(active_chat_subscriptions[channel], c.RemoteAddr().String())
	})

	underlying_connection, _ := upgrader.Upgrade(w, r, nil)
	subscription_client := underlying_connection.(*websocket.Conn)

	if active_chat_subscriptions[channel] == nil {
		active_chat_subscriptions[channel] = map[string]*websocket.Conn{}
	}

	if active_chat_subscriptions[channel][subscription_client.RemoteAddr().String()] == nil {
		recent := get_chat(channel)

		for i := len(recent) -1; i >= 0; i-- {
			subscription_client.WriteMessage(websocket.TextMessage, []byte(recent[i]))
		}

		active_chat_subscriptions[channel][subscription_client.RemoteAddr().String()] = subscription_client
	}
}

func feed_point(w http.ResponseWriter, r *http.Request) {
	upgrader := websocket.NewUpgrader()
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	upgrader.OnMessage(func(c *websocket.Conn, message_type websocket.MessageType, data []byte) {
		msg := strings.Split(string(data), ",")

		switch msg[0] {
		case "feed down":

			var feed *Feed
			_dbres_feed := lookup("feeds", msg[1])
			json.Unmarshal([]byte(_dbres_feed), &feed)
			feed.Down = true
			data, _ := json.Marshal(feed)
			put("feeds", []byte(msg[1]), data)

			var feed_sum *Feed_Sum
			_dbres_feedsum := lookup("feed_sum", "sum")
			json.Unmarshal([]byte(_dbres_feedsum), &feed_sum)			

			feed_sum.TotalValid = feed_sum.TotalValid -1
			data, _ = json.Marshal(feed_sum)
			put("feed_sum", []byte("sum"), data)

			return
			
		default:
			break
		}
		
		c.WriteMessage(message_type, random_feed())
	})
	
	conn, _ := upgrader.Upgrade(w, r, nil)	
	wsConn := conn.(*websocket.Conn)
	wsConn.SetReadDeadline(time.Time{})
}

func broadcast(data []byte) {
	for _, subscription_client := range active_overlay_subscriptions {
		subscription_client.WriteMessage(websocket.TextMessage, data)
	}
}

func main() {

	// var err error
	
	app_db = init_db ()
	//preload_db ()
	ch ()
	
	app := init_app ()

	app.Build()

	// pub, _  := os.ReadFile("cert.pem")
	// priv, _ := os.ReadFile("priv.key")
	// cert, _ := tls.X509KeyPair(pub, priv)

	service := nbhttp.NewServer(nbhttp.Config{
		Network: "tcp",
		//AddrsTLS:  []string{"0.0.0.0:443"},
		//TLSConfig: &tls.Config{ Certificates: []tls.Certificate{cert} },
		Addrs: []string{"0.0.0.0:5004"},
		Handler: app,
	})

	wsmux := &http.ServeMux{}
	wsmux.HandleFunc("/overlay", overlay_subscription)
	wsmux.HandleFunc("/thumbnail", thumbnail_subscription)
	wsmux.HandleFunc("/chat/", chat_subscription)
	wsmux.HandleFunc("/feed_point", feed_point)

	socket := nbhttp.NewServer(nbhttp.Config{
		Network: "tcp",
		Addrs: []string{"0.0.0.0:7009"},
		Handler: wsmux,
	})

	inbound := nbio.NewGopher(nbio.Config{
		Network: "udp",
		Addrs: []string{"0.0.0.0:7008"},
	})

	inbound.OnData(func(c *nbio.Conn, data []byte) {
		payload := strings.SplitN(string(data), ",", 4)
		if payload[0] == "xIMGx" {
			bucket := strings.SplitN(payload[1], "=", 2)[1]
			key := strings.SplitN(payload[2], "=", 2)[1]
			if(len(payload[3]) > 0) {
				put_thumbnail(bucket, key, payload[3])
			}
		} else {
			broadcast(data)
		}
	})

	socket.Start()
	defer socket.Stop()

	inbound.Start()
	defer inbound.Stop()

	service.Start()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	<-interrupt

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	service.Shutdown(ctx)
}
