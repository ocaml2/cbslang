package main

import (
	"encoding/json"
)

func feed_sum() *Feed_Sum {
	var feed_sum *Feed_Sum
	_dbres_feedsum := lookup("feed_sum", "sum")

	if _dbres_feedsum == "" {
		feed_sum = &Feed_Sum{ TotalValid: 0, NoName: 0, Duplicate: 0 }
	} else {
		json.Unmarshal([]byte(_dbres_feedsum), feed)
	}
	
	return feed_sum
}

func put_feed(name string, data []byte, feed_sum *Feed_Sum, include bool) {
	if name == "" {
		feed_sum.NoName = feed_sum.NoName +1
		return
	} else if lookup("feeds", name) != "" {
		feed_sum.Duplicate = feed_sum.Duplicate +1
		return
	}

	if include {
		feed_sum.TotalValid = feed_sum.TotalValid +1
	}
	
	put("feeds", []byte(name), data)
}

func preload_db () {
	iptv_all ()
	broadcast_all ()
}



