package main

import (
	"encoding/json"
)

func broadcast_all () {

	feed_sum := feed_sum ()

	var feed *Feed
	var data []byte

	feed = &Feed {
		Name: "kalo",
		Main: "static/images/placeholders/1280x960.png",
		Seq: []string{},
		IsBroadcast: true,
	}
	data, _ = json.Marshal(feed)
	put_feed(feed.Name, data, feed_sum, false)

	feed = &Feed {
		Name: "afro",
		Main: "static/images/placeholders/1280x960.png",
		Seq: []string{},
		IsBroadcast: true,
	}
	data, _ = json.Marshal(feed)
	put_feed(feed.Name, data, feed_sum, false)

	feed = &Feed {
		Name: "abc-la",
		Main: "static/images/placeholders/1280x960.png",
		Seq: []string{},
		IsBroadcast: true,
	}
	data, _ = json.Marshal(feed)
	put_feed(feed.Name, data, feed_sum, false)

	feed = &Feed {
		Name: "nbc-charlotte",
		Main: "static/images/placeholders/1280x960.png",
		Seq: []string{},
		IsBroadcast: true,
	}
	data, _ = json.Marshal(feed)
	put_feed(feed.Name, data, feed_sum, false)

}
