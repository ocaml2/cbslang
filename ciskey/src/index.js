import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, signInAnonymously, GoogleAuthProvider, signInWithPopup, onAuthStateChanged } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDaqGJbDNiYUAqtPeTDNm6q0egmWIbLpMU",
    authDomain: "ciskey.firebaseapp.com",
    projectId: "ciskey",
    storageBucket: "ciskey.appspot.com",
    messagingSenderId: "122956656772",
    appId: "1:122956656772:web:a6be990227dd5498ff12d1",
    measurementId: "G-5EX2F7NZ6T"
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const auth = getAuth(app);

const google_auth = new GoogleAuthProvider();

let google_sign_in = () => {
    signInWithPopup(auth, google_auth)
	.then((result) => {
	    const credential = GoogleAuthProvider.credentialFromResult(result);
	    const token = credential.accessToken;
	    const user = result.user;
	}).catch((error) => {
	    const error_code = error.code;
	    const error_message = error.message;
	    console.log(error);
	})
};
window.google_sign_in = google_sign_in;

let anon_sign_in = () => {
    signInAnonymously(auth)
	.then(() => {
	    
	})
	.catch((error) => {
	    const error_code = error.code;
	    const error_msg = error.message;
	});
};
window.anon_sign_in = anon_sign_in;

onAuthStateChanged(auth, (viewer) => {
    if (viewer) {
	sign_in(viewer);
    } else {
	sign_out();
	console.log("Successfully logged out")
    }
});


    
