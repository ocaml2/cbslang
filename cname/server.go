package main

import (
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"context"
	"net"
	"fmt"
	"time"
	"strings"
	"net/http"
	"github.com/lesismal/nbio"
)

/*

 █████ ██████   █████ █████ ███████████
░░███ ░░██████ ░░███ ░░███ ░█░░░███░░░█
 ░███  ░███░███ ░███  ░███ ░   ░███  ░ 
 ░███  ░███░░███░███  ░███     ░███    
 ░███  ░███ ░░██████  ░███     ░███    
 ░███  ░███  ░░█████  ░███     ░███    
 █████ █████  ░░█████ █████    █████   
░░░░░ ░░░░░    ░░░░░ ░░░░░    ░░░░░    
                                       
*/

type Stream struct {
	cmd *exec.Cmd
	cancel context.CancelFunc
}

var resources map[string]int64
var streams   map[string]Stream
var locations map[string]string

func add_vod_headers(dynamic http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		element := r.URL.Path

		core := strings.Split(element, "/")
		resource := strings.Join(core[2:len(core) -1], "/")

		if locations[resource] == "" {
			return
		}
		
		resources[resource] = time.Now().Unix()

		if streams[resource].cmd == nil {
			fmt.Println("Stream hasn't been started yet")

			ctx, cancelFunction := context.WithCancel(context.Background())
			
			stream := exec.CommandContext(ctx, "/home/circle/tsacr/main", locations[resource], "--quiet")
			wd, _ := os.Getwd()
			stream.Dir = wd + "/hls/" + resource

			stream.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
			
			fmt.Println(stream.Dir)
			stream.Start()

			streams[resource] = Stream{ cmd: stream, cancel: cancelFunction }
		} 
		
		var mime string
		switch strings.Split(element, ".")[1]{
		case "m3u8" : mime = "application/vnd.apple.mpegurl"
		case "ts"   : mime = "video/MP2T"
		case "mp4"  : mime = "video/mp4"
		case "mp3"  : mime = "audio/mp3"
		default     : mime = "unknown"
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Content-Type", mime)

		dynamic.ServeHTTP(w,r)
	}
}

func main() {

	resources = map[string]int64{}
	streams = map[string]Stream{}
	locations = map[string]string {
		"demo/nbc": "239.1.1.2:59002",
		"demo/bar": "239.1.1.12:59012",
		"demo/abc": "239.1.1.1:59001",
	}
	
	server_addr := "172.21.32.221:7008"	
	outbound := nbio.NewGopher(nbio.Config{})

	outbound.Start()
	defer outbound.Stop()
	
	server_conn, _ := net.Dial("udp", server_addr)
	nbc, _ := outbound.AddConn(server_conn)

	inbound := nbio.NewGopher(nbio.Config{
		Network: "udp",
		Addrs: []string{"127.0.0.1:7004"},
	})

	inbound.OnData(func(c *nbio.Conn, data []byte) {
		nbc.Write(data)
	})

	inbound.Start()
	defer inbound.Stop()	
	
	dynamic := &http.ServeMux{}
	dynamic.Handle("/dynamic/", add_vod_headers(http.StripPrefix("/dynamic", http.FileServer(http.Dir("./hls")))))
	
	// pub, _  := os.ReadFile("cert.pem")
	// priv, _ := os.ReadFile("priv.key")
	// cert, _ := tls.X509KeyPair(pub, priv)

	service := &http.Server{
		Addr: "0.0.0.0:6001",
		Handler: dynamic,
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go service.ListenAndServe()

	feeds_up := time.NewTicker(time.Second * 60)
	done := make(chan bool)

	go func() {
		for {
			select {
			case t := <-feeds_up.C:
				now := t.Unix()
				for resource, last := range resources {
					if now - last > 60 {
						fmt.Println(resource + " hasn't been accessed in a while, stopping stream")
						delete(resources, resource)
						_ = syscall.Kill(-streams[resource].cmd.Process.Pid, syscall.SIGTERM)
						delete(streams, resource)

						
						
					} 
				}

			case <-done:
				return
			}
			
		}
	}()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	<-interrupt

	feeds_up.Stop()
	done <- true

	for _, stream := range streams {
		syscall.Kill(-stream.cmd.Process.Pid, syscall.SIGTERM)
		
	}
}
